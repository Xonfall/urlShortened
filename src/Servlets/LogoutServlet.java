package Servlets;

import Core.Validator.FormValidator;
import Core.Validator.MailValidator;
import Dao.Factory.DaoFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "LogOut", urlPatterns = "/logOut")
public class LogoutServlet extends HttpServlet {
    private final String HOME_VIEW = "/views/index.jsp";

    protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

    }

    protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        HttpSession session=request.getSession();
        session.invalidate();
        request.getRequestDispatcher(this.HOME_VIEW).forward(request, response);
    }
}
