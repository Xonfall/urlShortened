package Servlets.Url;

import Bean.SettingBean;
import Core.ClickCore;
import Dao.Factory.BeanFactory;
import Dao.Factory.DaoFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "urlLoginServlet", urlPatterns = "/url/login")
public class LoginServlet extends HttpServlet {
    private final String URL_LOGIN_VIEW = "/views/urls/login.jsp";
    private final String HOME_VIEW = "/views/index.jsp";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BeanFactory bean = new BeanFactory();
        DaoFactory dao = new DaoFactory();
        List<String> passwords;
        boolean success = false;

        String passwordParameter = request.getParameter("password");
        boolean haveSettings = (boolean) request.getSession().getAttribute("haveSettings");

        if (!passwordParameter.isEmpty()) {
            try {
                passwords = dao.getUrlDao().findUrlWithPasswords(bean.getUrlBean(), request.getSession().getAttribute("urlRequest").toString());
                SettingBean settingBean = dao.getSettingDao().find(bean.getSettingBean(), request.getSession().getAttribute("urlRequest").toString());

                for (String password : passwords) {
                    if (password.equals(passwordParameter)) {
                        if (haveSettings) {
                            if (ClickCore.isValid(settingBean)) {
                                dao.getSettingDao().updateClickUsed(settingBean);

                                success = true;

                                response.sendRedirect(request.getSession().getAttribute("urlResponse").toString());
                            } else {
                                success = true; // pour tricher et ne pas rediriger deux fois sur la home sinon error 500
                                request.setAttribute("error", "La validité de l'url a expiré !");

                                request.getServletContext().getRequestDispatcher(HOME_VIEW).forward(request, response);
                            }
                        } else {
                            response.sendRedirect(request.getSession().getAttribute("urlResponse").toString());

                            success = true;
                        }
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
                success = false;
            }

            if (!success) {
                request.setAttribute("error", "Ce n'est pas le bon mot de passe !");
                this.getServletContext().getRequestDispatcher(URL_LOGIN_VIEW).forward(request, response);

            }
        } else {
            request.setAttribute("error", "Le champ est vide !");
            this.getServletContext().getRequestDispatcher(URL_LOGIN_VIEW).forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
    }
}
