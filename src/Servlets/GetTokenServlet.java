package Servlets;

import Dao.Factory.BeanFactory;
import Dao.Factory.DaoFactory;
import Dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "getToken", urlPatterns = "/getToken")
public class GetTokenServlet extends HttpServlet {
    private final String URL_LOGIN_VIEW = "/views/getToken.jsp";

    protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

    }

    protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        DaoFactory dao = new DaoFactory();

        String getToken = request.getParameter("token");
        Integer getId = dao.getUserDao().findByToken(getToken);

        if (getToken.isEmpty()) {
            request.setAttribute("tokenActivation", "Ok frr il y a un problème");
            request.getRequestDispatcher(this.URL_LOGIN_VIEW).forward(request, response);
        } else {
            if (dao.getUserDao().updateById(getId)) {
                request.setAttribute("tokenActivation", "Ok frr ton compte est activé");
                request.getRequestDispatcher(this.URL_LOGIN_VIEW).forward(request, response);
            } else {
                request.setAttribute("tokenActivation", "Ok frr il y a un problème");
                request.getRequestDispatcher(this.URL_LOGIN_VIEW).forward(request, response);
            }
        }



    }
}
