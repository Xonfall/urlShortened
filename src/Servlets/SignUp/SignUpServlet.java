package Servlets.SignUp;


import Core.Validator.FormValidator;
import Core.Validator.MailValidator;
import Dao.Factory.DaoFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "SignUpServlet", urlPatterns = "/signUp")
public class SignUpServlet extends HttpServlet {
    private final String VIEW = "/views/users/sign.jsp";
    private final String HOME_VIEW = "/views/index.jsp";


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> formParameters = new HashMap<>();

        DaoFactory dao = new DaoFactory();

        String mail = request.getParameter("mail");
        String passwordParameter = request.getParameter("password");
        String business = request.getParameter("business");

        formParameters.put("mail", mail);
        formParameters.put("password", passwordParameter);
        formParameters.put("business", business);

        FormValidator formValidator = new FormValidator(formParameters);

        if (formValidator.signUp()) {
            if (MailValidator.isValid(mail)) {
                if (dao.getUserDao().create(formParameters)) {
                    this.getServletContext().getRequestDispatcher(HOME_VIEW).forward(request, response);
                } else {
                    request.setAttribute("error", "Une erreur est survenue !");

                    this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
                }
            } else {
                request.setAttribute("error", "Votre email n'est pas valide !");

                this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
            }
        } else {
            request.setAttribute("error", "Les champs sont obligatoire !");

            this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher(this.VIEW).forward(request, response);
    }
}
