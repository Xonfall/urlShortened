package Core;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionCore {

    private static HttpServletRequest request = null;
    private static HttpSession session = null;

    public SessionCore( HttpServletRequest request, HttpSession session ) {
        SessionCore.request = request;
        SessionCore.session = session;
    }

    public static int returnUserId() {
        if( SessionCore.request.getSession().getAttribute("id") != null) {
            return (int) SessionCore.request.getSession().getAttribute("id");
        } else {
            return 1;
        }
    }

}
