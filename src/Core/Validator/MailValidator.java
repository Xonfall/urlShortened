package Core.Validator;


import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class MailValidator {

    public static boolean isValid(String email) {
        InternetAddress mailAddress;

        try {
            mailAddress = new InternetAddress(email);
            mailAddress.validate();

            return true;
        } catch (AddressException e) {
            e.printStackTrace();
        }
        return false;
    }
}
