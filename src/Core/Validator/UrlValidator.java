package Core.Validator;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlValidator {
    public static boolean isValid(String url) {
        try {
            new URL(url);

            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    /*URI uri = new URI(url);
    String domain = uri.getHost();
    return domain.startsWith("www.") ? domain.substring(4) : domain;*/
}