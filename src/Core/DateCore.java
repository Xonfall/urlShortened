package Core;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateCore {

    public static Timestamp convertStringToDate(String string) {
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parsedDate = dateFormat.parse(string);
            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            return timestamp;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean checkDates(String dateStart, String dateEnd) {
        Timestamp start = convertStringToDate(dateStart);
        Timestamp end = convertStringToDate(dateEnd);

        return !start.after(end) && !start.after(new Date()) && !end.before(start) && !end.before(new Date());
    }

    public static boolean checkDates(Timestamp dateStart, Timestamp dateEnd) {
        if (dateStart != null && dateEnd != null) {
            return !dateStart.after(dateEnd) && !dateStart.after(new Date()) && !dateEnd.before(dateStart) && !dateEnd.before(new Date());
        }

        return false;
    }
}
