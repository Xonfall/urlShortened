package Filter;

import Bean.SettingBean;
import Bean.UrlBean;
import Core.ClickCore;
import Core.DateCore;
import Dao.Factory.BeanFactory;
import Dao.Factory.DaoFactory;
import Dao.SettingDao;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UrlFilter implements Filter {
    private final String URL_LOGIN_VIEW = "/views/urls/login.jsp";
    private final String HOME_VIEW = "/views/index.jsp";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        // HttpSession session = request.getSession();


        BeanFactory bean = new BeanFactory();
        DaoFactory dao = new DaoFactory();

        String urlRequest = request.getRequestURL().toString();

        UrlBean urlResponse = dao.getUrlDao().find(bean.getUrlBean(), urlRequest);
        SettingBean settingBean = dao.getSettingDao().find(bean.getSettingBean(), urlRequest);

        if (urlResponse != null) {
            if (settingBean != null) {
                // a une date met pas de click et pas de mdp
                if(settingBean.getStartedAt() != null && settingBean.getExpiredAt() != null && settingBean.getDownloadLimit() == 0 && !dao.getUrlDao().urlHavePassword(urlResponse, urlRequest)) {
                    if (DateCore.checkDates(settingBean.getStartedAt(), settingBean.getExpiredAt())) {
                        response.sendRedirect(urlResponse.getUrl());
                    } else {
                        request.setAttribute("error", "La validité de l'url a expiré !");

                        request.getServletContext().getRequestDispatcher(HOME_VIEW).forward(request, response);
                    }
                    // pas de date met à le click et pas de mdp
                } else if (settingBean.getStartedAt() == null && settingBean.getExpiredAt() == null && settingBean.getDownloadLimit() != 0 && !dao.getUrlDao().urlHavePassword(urlResponse, urlRequest)) {
                    if (ClickCore.isValid(settingBean)){
                        dao.getSettingDao().updateClickUsed(settingBean);

                        response.sendRedirect(urlResponse.getUrl());
                    } else {
                        request.setAttribute("error", "La validité de l'url a expiré !");

                        request.getServletContext().getRequestDispatcher(HOME_VIEW).forward(request, response);
                    }
                    // a la date et le click et pas de mdp
                } else if (settingBean.getStartedAt() != null && settingBean.getExpiredAt() != null && settingBean.getDownloadLimit() != 0 && !dao.getUrlDao().urlHavePassword(urlResponse, urlRequest)) {
                    if (DateCore.checkDates(settingBean.getStartedAt(), settingBean.getExpiredAt()) && ClickCore.isValid(settingBean)) {
                        dao.getSettingDao().updateClickUsed(settingBean);

                        response.sendRedirect(urlResponse.getUrl());
                    } else {
                        request.setAttribute("error", "La validité de l'url a expiré !");

                        request.getServletContext().getRequestDispatcher(HOME_VIEW).forward(request, response);
                    }
                    // pas de date, pas de mdp mais a le click
                } else if (settingBean.getStartedAt() == null && settingBean.getExpiredAt() == null && settingBean.getDownloadLimit() != 0 && !dao.getUrlDao().urlHavePassword(urlResponse, urlRequest)) {
                    dao.getSettingDao().updateClickUsed(settingBean);

                    response.sendRedirect(urlResponse.getUrl());
                }


                if (settingBean.getStartedAt() != null && settingBean.getExpiredAt() != null && settingBean.getDownloadLimit() == 0) {
                    // Si il y a une date et un mdp on continu
                    if (DateCore.checkDates(settingBean.getStartedAt(), settingBean.getExpiredAt())) {
                        if (dao.getUrlDao().urlHavePassword(urlResponse, urlRequest)) {
                            session.setAttribute("urlRequest", urlRequest);
                            session.setAttribute("urlResponse", urlResponse.getUrl());
                            session.setAttribute("haveSettings", false);

                            request.getServletContext().getRequestDispatcher(URL_LOGIN_VIEW).forward(request, response);
                        }
                    } else {
                        request.setAttribute("error", "La validité de l'url a expiré !");

                        request.getServletContext().getRequestDispatcher(HOME_VIEW).forward(request, response);
                    }
                    // a date, click et mdp
                } else if (settingBean.getStartedAt() != null && settingBean.getExpiredAt() != null && settingBean.getDownloadLimit() != 0 && dao.getUrlDao().urlHavePassword(urlResponse, urlRequest)) {
                    if (DateCore.checkDates(settingBean.getStartedAt(), settingBean.getExpiredAt()) && ClickCore.isValid(settingBean)) {
                        if (dao.getUrlDao().urlHavePassword(urlResponse, urlRequest)) {
                            session.setAttribute("urlRequest", urlRequest);
                            session.setAttribute("urlResponse", urlResponse.getUrl());
                            session.setAttribute("haveSettings", true);

                            request.getServletContext().getRequestDispatcher(URL_LOGIN_VIEW).forward(request, response);
                        }
                    } else {
                        request.setAttribute("error", "La validité de l'url a expiré !");

                        request.getServletContext().getRequestDispatcher(HOME_VIEW).forward(request, response);
                    }
                }
                //pas de date met a le click
                else if (settingBean.getStartedAt() == null && settingBean.getExpiredAt() == null && settingBean.getDownloadLimit() != 0){
                    if (dao.getUrlDao().urlHavePassword(urlResponse, urlRequest)) {
                        session.setAttribute("urlRequest", urlRequest);
                        session.setAttribute("urlResponse", urlResponse.getUrl());
                        session.setAttribute("haveSettings", true);

                        request.getServletContext().getRequestDispatcher(URL_LOGIN_VIEW).forward(request, response);
                    }
                }
            } else {
                if (dao.getUrlDao().urlHavePassword(urlResponse, urlRequest)) {
                    session.setAttribute("urlRequest", urlRequest);
                    session.setAttribute("urlResponse", urlResponse.getUrl());
                    session.setAttribute("haveSettings", false);

                    request.getServletContext().getRequestDispatcher(URL_LOGIN_VIEW).forward(request, response);
                } else {
                    response.sendRedirect(urlResponse.getUrl());
                }
            }
            //filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
