package Dao;

import Bean.PasswordBean;
import Core.DatabaseCore;
import Dao.Abstract.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class PasswordDao extends Dao<PasswordBean, String> {
    private PreparedStatement query;
    private ResultSet result;

    /**
     *
     * @param object
     * @param urlShort
     * @return PasswordBean
     */
    @Override
    public PasswordBean find(PasswordBean object, String urlShort) {
        try {
            this.query = DatabaseCore.getInstance().prepareStatement("SELECT password, active FROM "+ object.getTableName() +" WHERE url_short = ?;");
            this.query.setString(1, urlShort);

            result = this.query.executeQuery();

            if (result.next()) {
                object.setPassword(result.getString("password"));
                object.setActive(result.getInt("active"));

                return object;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new NullPointerException();
    }

    /**
     *
     * @param object
     * @param form
     * @return
     */
    @Override
    public PasswordBean create(PasswordBean object, Map<String, String> form) {
        return null;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public PasswordBean edit(PasswordBean object) {
        return null;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public PasswordBean update(PasswordBean object) {
        return null;
    }

    /**
     *
     * @param object
     */
    @Override
    public void delete(PasswordBean object) {

    }
}
