package Dao;

import Bean.UserBean;
import Core.DatabaseCore;
import Core.MailCore;
import Core.SessionCore;
import Core.Sha1Core;
import Dao.Abstract.Dao;
import Dao.Factory.DaoFactory;

import java.security.NoSuchAlgorithmException;


import java.security.SecureRandom;
import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

public class UserDao extends Dao<UserBean, String> {

    private PreparedStatement query;
    private ResultSet result;


    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd = new SecureRandom();

    private String randomString( int len ) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    /**
     *
     * @param object
     * @param object2
     * @return
     */
    @Override
    public UserBean find( UserBean object, String object2 ) {
        return null;
    }

    /**
     *
     * @param object
     * @param form
     * @return
     */
    @Override
    public UserBean create( UserBean object, Map<String, String> form ) {

        try {
            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("INSERT INTO " + object.getTableName() + " (mail, password, active, created_at, updated_at, business, token) VALUES (?, ?, ?, ?, ?, ?, ?);");

            this.query.setString(1, form.get("mail"));
            this.query.setString(2, Sha1Core.sha1(form.get("password")));
            this.query.setInt(3, 0);
            this.query.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            this.query.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
            this.query.setString(6, form.get("business"));
            this.query.setString(7, randomString(60));

            this.query.executeUpdate();


            object.setMail(form.get("mail"));


            return object;
        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


        return object;
    }

    /**
     *
     * @param form
     * @return
     */
    public boolean create( Map<String, String> form ) {
        String random = randomString(100);
        try {

            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("INSERT INTO users (mail, password, active, created_at, updated_at, business, token) VALUES (?, ?, ?, ?, ?, ?, ?);");

            this.query.setString(1, form.get("mail"));
            this.query.setString(2, Sha1Core.sha1(form.get("password")));
            this.query.setInt(3, 0);
            this.query.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            this.query.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
            this.query.setString(6, form.get("business"));
            this.query.setString(7, random);

            this.query.executeUpdate();

            MailCore.sendMail(form.get("mail"), random);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        // MailCore.sendM();

        return false;
    }

    /**
     *
     * @param token
     * @return
     */
    public int findByToken( String token ) {
        try {
            this.query = DatabaseCore.getInstance().prepareStatement("SELECT * FROM users WHERE token = ?;");
            this.query.setString(1, token);

            result = this.query.executeQuery();

            if (result.next()) {
                return result.getInt("id");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new NullPointerException();

    }

    /**
     *
     * @param id
     * @return
     */
    public ArrayList<String> findById( Integer id ) {
        try {
            this.query = DatabaseCore.getInstance().prepareStatement("SELECT * FROM users WHERE id = ?;");
            this.query.setInt(1, id);

            result = this.query.executeQuery();

            ResultSetMetaData metadata = result.getMetaData();
            int columnCount = metadata.getColumnCount();

            ArrayList<String> columns = new ArrayList<String>();
            ArrayList<String> value = new ArrayList<String>();
            for (int i = 1; i < columnCount; i++) {
                String columnName = metadata.getColumnName(i);
                columns.add(columnName);
            }

            while (result.next()) {
                for (String columnName : columns) {
                    value.add(result.getString(columnName));
                }
            }

            return value;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new NullPointerException();

    }

    /**
     *
     * @param id
     * @return
     */
    public boolean updateById( Integer id ) {

        try {
            this.query = DatabaseCore.getInstance().prepareStatement("UPDATE users SET active = '1' WHERE id = ?;");
            this.query.setInt(1, id);
            this.query.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public UserBean edit( UserBean object ) {
        return null;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public UserBean update( UserBean object ) {
        return null;
    }

    /**
     *
     * @param form
     * @return
     */
    public boolean update( Map<String, String> form ) {

        try {
            this.query = DatabaseCore.getInstance().prepareStatement("UPDATE users SET password = ?, business = ? WHERE id = ?;");


                this.query.setString(1, Sha1Core.sha1(form.get("password")));

                this.query.setString(2, form.get("business"));

            this.query.setInt(3, SessionCore.returnUserId());

            this.query.executeUpdate();


            return true;
        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     *
     * @param object
     */
    @Override
    public void delete( UserBean object ) {

    }

    /**
     *
     * @param form
     * @return
     */
    public int findByLogin( Map<String, String> form ) {
        try {
            this.query = DatabaseCore.getInstance().prepareStatement("SELECT * FROM users WHERE mail = ? AND password = ?;");
            this.query.setString(1, form.get("mail"));
            this.query.setString(2, Sha1Core.sha1(form.get("password")));

            result = this.query.executeQuery();

            if (result.next()) {
                return result.getInt("id");
            }

        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
