package Dao.Factory;

import Dao.PasswordDao;
import Dao.SettingDao;
import Dao.UrlDao;
import Dao.UserDao;

public class DaoFactory {

    public UrlDao getUrlDao() {
        return new UrlDao();
    }
    public PasswordDao getPasswordDao() {
        return new PasswordDao();
    }
    public UserDao getUserDao() { return new UserDao(); }
    public SettingDao getSettingDao() { return new SettingDao(); }
}
