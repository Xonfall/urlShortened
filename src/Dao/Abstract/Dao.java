package Dao.Abstract;

import java.util.Map;

public abstract class Dao<T, S> {

    public abstract T find(T object, S object2);

    public abstract T create(T object, Map<String, String> form);

    public abstract T edit(T object);

    public abstract T update(T object);

    public abstract void delete(T object);

}