package Bean;

import java.io.Serializable;

public class PasswordBean implements Serializable {
    private String mail;
    private String password;
    private int active;
    private int id_url;
    private String tableName = "passwords";

    public PasswordBean() {}

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getId_url() {
        return id_url;
    }

    public void setId_url(int id_url) {
        this.id_url = id_url;
    }

    public String getTableName() {
        return tableName;
    }
}
