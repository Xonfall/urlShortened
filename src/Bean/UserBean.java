package Bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class UserBean implements Serializable {
    private String mail;
    private String password;
    private String business;
    private int active;
    private Timestamp created_at;
    private Timestamp updated_at;
    private String token;
    private final String tableName = "users";

    public UserBean() {
    }

    public String getMail() {
        return mail;
    }

    public void setMail( String mail ) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword( String password ) {
        this.password = password;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness( String business ) {
        this.business = business;
    }

    public int getActive() {
        return active;
    }

    public void setActive( int active ) {
        this.active = active;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at( Timestamp created_at ) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at( Timestamp updated_at ) {
        this.updated_at = updated_at;
    }

    public String getToken() {
        return token;
    }

    public void setToken( String token ) {
        this.token = token;
    }


    public String getTableName() {
        return tableName;
    }
}
