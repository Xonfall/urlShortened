package Bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class UrlBean implements Serializable {
    private String url;
    private String url_short;
    private Timestamp created_at;
    private final String tableName = "urls";

    public UrlBean() {}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl_short() {
        return url_short;
    }

    public void setUrl_short(String url_short) {
        this.url_short = url_short;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public String getTableName() {
        return tableName;
    }
}
