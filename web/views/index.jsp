<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="layout/header.jsp"/>

<h1>Accueil</h1>
${msg}<a href="${url}">${url}</a>
${error}
<div class="row">
    <div class="col align-self-center">
        <form action="/url" method="post" class="form-group">
            <label for="url_shortener">Url :</label>
            <input id="url_shortener" type="text" name="url">

            <div id="passwords">
                <label for="password_1">Mot de passe :</label>
                <input id="password_1" type="password" name="password_1">

                <label for="password_2">Mot de passe 2 :</label>
                <input id="password_2" type="password" name="password_2">

                <label for="password_3">Mot de passe 3 :</label>
                <input id="password_3" type="password" name="password_3">
            </div>

            <label for="start">Date de début</label>
            <input type="date" id="start" name="date-start">

            <label for="end">Date de fin</label>
            <input type="date" id="end" name="date-end">

            <label for="limit">Nombre d'accès autorisé</label>
            <input type="number" id="limit" name="limit">

            <div id="recaptcha"></div>

            <button type="submit" class="btn btn-info">Valider</button>
        </form>
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
        <script type="text/javascript">
           /* var buttonField = document.getElementById('generator');
            var passwordField = document.getElementById('password_1');
            var container = document.getElementById('passwords');

            buttonField.onclick = addField();

            function addField() {
                var pattern = 'password_';

                if (container.getElementsByTagName('input') != null) {
                    var clone = container.getElementsByTagName('input');
                    var idValue = clone.id.replace( /^\D+/g, '');
                    console.log(idValue)
                    clone.id = pattern + (idValue + 1);
                    console.log(clone);
                }

                //container.appendChild(clone);
            }*/

        </script>
    </div>
</div>


<jsp:include page="layout/footer.jsp"/>