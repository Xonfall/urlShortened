<%--
  Created by IntelliJ IDEA.
  User: xonfall
  Date: 11/04/2018
  Time: 22:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="../layout/header.jsp"/>

<h1>Connexion</h1>
${msg}
${error}

<form action="logIn" method="post">
    <label for="mail">Mail :</label>
    <input id="mail" type="text" name="mail">

    <label for="password">Mot de passe :</label>
    <input id="password" type="password" name="password">


    <button type="submit">Se connecter</button>
</form>

<jsp:include page="../layout/footer.jsp"/>