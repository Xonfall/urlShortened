<html>
    <head>
        <title>$Title$</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/jquery.js"></script>



    <script type="text/javascript">
    var onloadCallback = function() {
    grecaptcha.render('recaptcha', {
    'sitekey' : '6LcUxl8UAAAAAOB4QozwBYyO_W8EGeAG22XkaFsz'
    });
    };
    </script>
    </head>

<body>
    <nav>
    <jsp:useBean id="urlBean" class="Bean.UrlBean"/>
    <jsp:setProperty name="urlBean" property="url" param="url"/>
    <a href="/url">lien</a>
        <% if(session.getAttribute("id") == null) { %>
    <a href="/signUp">inscription</a>
    <a href="/logIn">Se connecter</a>
        <% }  else { %>
        <a href="/account">Mon compte</a>
    <% } %>
    </nav>
    <div class="container">